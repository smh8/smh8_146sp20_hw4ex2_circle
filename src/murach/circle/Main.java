package murach.circle;

import java.util.Scanner;

/**
 * This is an application that takes user inputed radii
 * and prints out the Radius, Area, Circumference, 
 * and Diameter
 * 
 * @author smh8
 **/
public class Main {

    /**
     * The main method
     * 
     * @param args 
     */
    public static void main(String[] args) {
        System.out.println("Welcome to the Circle Calculator");
        System.out.println();

        Scanner sc=new Scanner(System.in);
        
        String choice="y";
        
        while (choice.equals("y")) {
            // get input from user
            /*
            System.out.print("Enter radius:  ");
            double radius = Double.parseDouble(sc.nextLine());
            */
            
            /*
            HW4 Ex2, step 7: Modify the code that gets the radius
            from the user so it prompts the user to enter one or 
            moreradiuses (or "radii," if you prefer) on the same line, 
            with each radius separated by a space. Store this line in a 
            String variable called line.
            */
            System.out.print("Enter Radii: ");
            String line=(sc.nextLine());
            
            /*
            HW4 Ex2, step 8: Modify the code so it parses
            the line of text that the user enters to get a 
            1-Darray of String objects,with one String object 
            for each entry(you can call the String array entries).
            */
            line=line.trim();
            String entries[]=line.split(" ");
            
            /*
            HW4 Ex2, step 9: Declare and create a new 1-D array
            of Circle object references (which you might call
            circles), such that the length of circles will have
            the same length as the entries array created above.
            */
            Circle circles[];
            circles=new Circle[entries.length];
            
            //Circle circle = new Circle(radius);
            
            /*
            HW4 Ex2, step 10: Write a counter-controlled for
            loop that iterates through each element in the 
            entries array. For each element in this array, 
            convert the String referred to by the current 
            entries element into a double, and store this 
            doublein a variable called radius. Use this value 
            of radiusas the argument to the "one-arg" Circle 
            constructor, and store a reference to your 
            newly-instantiated Circle object in the current 
            element of the circlesarray.
            */
            double radius;
            for(int x=0; x<entries.length; x++){
                radius=Double.parseDouble(entries[x]);
                circles[x]=new Circle(radius);
            } //end circle constructor
            
            /*
            HW4 Ex2, step 11: Create another loop that 
            iterates through the array of Circle objects 
            and displays the data for each Circle object 
            on the console. This data should include the 
            radius, area, circumference, and diameter, 
            each displayed on its own line. 
            */
            for(int x=0; x<circles.length; x++){
                // format and display output
                String message = 
                    "Radius:        " + 
                    circles[x].getRadius() + "\n" + 
                    "Area:          " + 
                    circles[x].getArea() + "\n" +
                    "Circumference: " + 
                    circles[x].getCircumference() + "\n" +
                    "Diameter:      " + 
                    circles[x].getDiameter() + "\n";
                System.out.println(message);
            } //end disp for

            // see if the user wants to continue5
            System.out.print("Continue? (y/n): ");
            choice = sc.nextLine();
            System.out.println();
        } //end while
        System.out.println("Bye!");
        sc.close();
    } //end main
} //end Main